App.Model = function () {
	// App.Model should be able to use methods of the EventPublisher, so 'that' will be an EventPublisher
	// This pattern is similar to Interfaces in Java.
	var that = new EventPublisher(),
		counter;

	function countUp(id) {
		var rand = Math.round((Math.random() * 4) + 1);
		counter += rand;

		// setTimeout is only used to simulate some processing time
		setTimeout(function () {
			onCountUp(id);
		}, 1000);
	}

	function onCountUp(id) {
		// {counter: counter, id: id} is an on-the-fly created object literal
		// to be passed as parameter for the observers' callbacks
		that.notifyAll('countedUp', {
			counter: counter,
			id: id
		});
	}

	function init(maxValue) {
		counter = maxValue;
		return that;
	}

	that.init = init;
	that.countUp = countUp;

	return that;
}