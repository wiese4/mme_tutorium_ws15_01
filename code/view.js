App.View = function () {
	// App.View should be able to use methods of the EventPublisher, so 'that' will be an EventPublisher
	// This pattern is similar to Interfaces in Java.
	var that = new EventPublisher(),
		list;

	function onListItemClick(event) {
		var target = event.target;

		// === should always be used instead of == for comparisons
		if (target.id === 'list') {
			return false;
		}
		that.notifyAll('listItemClicked', target.id[5]); // Characters in Strings can be accessed over their index in array-like brackets-notation
	}

	function updateView(data) {
		var item = document.getElementById('item-' + data.id);
		item.innerHTML = data.counter;
	}

	function initEvents() {
		list.addEventListener('click', onListItemClick, 'false');
	}

	function init() {
		list = document.getElementById('list');
		initEvents();
		return that;
	}

	that.init = init;
	that.updateView = updateView;

	return that;
}