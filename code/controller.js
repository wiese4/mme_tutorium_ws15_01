App.Controller = (function () {
	var that = {},
		model,
		view;

	function onListItemClicked(event) {
		model.countUp(event.data);
	}

	function onCountedUp(event) {
		view.updateView(event.data);
	}

	function initEvents() {
		view.addEventListener('listItemClicked', onListItemClicked);
		model.addEventListener('countedUp', onCountedUp);
	}

	function init() {
		view = App.View().init();
		model = App.Model().init(9); // magic number. Don't do that!
		initEvents();
	}

	that.init = init;

	return that;
})();