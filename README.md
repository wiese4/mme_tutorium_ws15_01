# MME Tutorium WS15 01
Diese Mini-App dient der Veranschaulichung einiger Grundkonzepte, die im Kurs MME an der Universität Regensburg relevant sind, u.a.

* Zusammenspiel von HTML, CSS, JS
* Module Pattern
* Immediately-Invoked Function Expression (IIFE, siehe z.B. https://developer.mozilla.org/en-US/docs/Glossary/IIFE)
* EventPublisher
* Model-View-Controller Pattern (MVC)

Die App sollte nicht als Musterbeispiel für die eigene Entwicklung herangezogen werden, da sie innerhalb einer Tutoriumseinheit gemeinsam mit den Kursteilnehmenden entstanden ist.